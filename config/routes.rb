Rails.application.routes.draw do

  get 'sessions/new'
  get 'sessions/create'
  get 'sessions/destroy'
  root to: 'main#index'

  get 'main/index'

  resources :design_reference, only: [:index] do
  	collection do
  	  get :register
    end
  end

  resources :users, only: [:create,:update] 
  get 'signup' => 'users#new', as: :new_user	

  resources :sessions, only: [:create]
  get 'logout' => 'sessions#destroy' 
  get 'login'  => 'sessions#new', as: :new_session

  resources :admin, only: [:index]

  namespace :admin do
    resources :companies
    resources :categories
  end  

end