Crummy.configure do |config|
  config.format = :html_list
  config.html_separator = ""
  config.ul_class = 'breadcrumb'
  config.last_crumb_linked = false
  config.li_class = 'breadcrumb-item'
end