class CategoriesGrid
  include Datagrid

  scope do
    Category
  end

  filter(:id, :string, :multiple => ',')
  filter(:name, :string)
  filter(:slug, :string)

  column(:id, :mandatory => true)
  column(:name, :mandatory => true)
  column(:slug, :mandatory => true)

  column(:actions, :html => true, :mandatory => true) do |record|
    link_to raw("Edit <i class='fa fa-edit'></i>"), edit_admin_category_path(record)
  end

end