class CompaniesGrid
  include Datagrid

  scope do
    Company
  end

  filter(:id, :string, :multiple => ',')
  filter(:name, :string)
  filter(:company_type, :enum, :select => Company::TYPE.invert)
  filter(:status, :enum, :select => Company::STATUS.invert)

  column(:id, :mandatory => true)
  column(:name, :mandatory => true)
  column(:company_type, :mandatory => true) do |model|
    format(model.status) do |value|
      Company::TYPE[model.company_type]
    end
  end
  column(:status, :mandatory => true) do |model|
    format(model.status) do |value|
      Company::STATUS[model.status]
    end
  end

  column(:actions, :html => true, :mandatory => true) do |record|
    link_to raw("Edit <i class='fa fa-edit'></i>"), edit_admin_company_path(record)
  end

end