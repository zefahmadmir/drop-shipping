class Role < ApplicationRecord
	has_and_belongs_to_many :users
	has_and_belongs_to_many :permissions

	def super_admin?
	  uid == 'super_admin'
	end
	
end
