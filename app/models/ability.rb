class Ability
  include CanCan::Ability

  def initialize(user)
    
    user.roles.includes(:permissions).each do |role|
        role.permissions.each do |p|
            can p.action.to_sym, p.klass.constantize, p.condition_hash
        end
        can :manage, :all if role.super_admin?
    end    

  end
end
