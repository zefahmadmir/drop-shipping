class User < ApplicationRecord
  authenticates_with_sorcery!
  delegate :can?, :cannot?, :to => :ability

  has_and_belongs_to_many :roles

  def ability
    @ability ||= Ability.new(self)
  end
  
end
