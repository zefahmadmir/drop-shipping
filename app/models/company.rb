class Company < ApplicationRecord
	extend FriendlyId
	friendly_id :uid, use: :slugged
	attr_accessor :slug

	belongs_to :city

	validates :name, :owner_id , :city_id, :address ,presence: true

	TYPE = { 1 => 'Seller' , 2 => 'Vendor' }
	STATUS = { 1 => 'Active' , 2 => 'InActive', 3 => 'Blocked' }

end
