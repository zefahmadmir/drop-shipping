class Category < ApplicationRecord
	extend ActsAsTree::TreeView
	acts_as_tree order: 'name'

	extend FriendlyId
    friendly_id :name, use: :slugged

end
