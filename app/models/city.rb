class City < ApplicationRecord

	validates :name, :slug , presence: true
	
end
