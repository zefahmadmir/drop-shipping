// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require rails-ujs
//= require activestorage



//= require ela/lib/jquery/jquery.min.js
//= require ela/lib/bootstrap/js/popper.min.js
//= require ela/lib/bootstrap/js/bootstrap.min.js
//= require ela/jquery.slimscroll.js
//= require ela/sidebarmenu.js
//= require ela/lib/sticky-kit-master/dist/sticky-kit.min.js
//= require ela/lib/morris-chart/raphael-min.js
//= require ela/lib/morris-chart/morris.js
//= require ela/lib/morris-chart/dashboard1-init.js
//= require ela/lib/calendar-2/moment.latest.min.js
//= require ela/lib/calendar-2/semantic.ui.min.js
//= require ela/lib/calendar-2/prism.min.js
//= require ela/lib/calendar-2/pignose.calendar.min.js
//= require ela/lib/calendar-2/pignose.init.js
//= require ela/lib/owl-carousel/owl.carousel.min.js
//= require ela/lib/owl-carousel/owl.carousel-init.js
//= require ela/scripts.js
//= require ela/custom.min.js
//= require parsley.min.js

// require_tree .
