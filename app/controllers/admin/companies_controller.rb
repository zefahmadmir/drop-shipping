class Admin::CompaniesController < AdminController
  
  load_and_authorize_resource only: [:edit,:update]
  
  def index
    @companies_grid = CompaniesGrid.new(params[:companies_grid]) do |scope|
      scope.page(params[:page])
    end
    add_crumb "Company Managment", admin_companies_path
  end

  def new
    @company = Company.new
    add_crumb "Create Company", admin_companies_path
  end

  def edit
    add_crumb "Update Company", admin_companies_path
  end

  def create
    @company = Company.new(company_params)
    if @company.save
      redirect_to admin_companies_path, notice: 'Company was successfully created.'
    else
      render :new
    end
  end

  def update
    if @company.update(company_params)
      redirect_to admin_companies_path, notice: 'Company was successfully updated.'
    else
      render :edit
    end
  end

  private

  def set_company
    @company = Company.find(params[:id])
  end

  def company_params
    params.require(:company).permit(:name,:company_type,:city_id,:address,:status,:owner_id)
  end
end
