class Admin::CategoriesController < AdminController

	load_and_authorize_resource only: [:edit,:update]

	def index
	  @categories_grid = CategoriesGrid.new(params[:categories_grid]) do |scope|
	    scope.page(params[:page])
	  end
	  add_crumb "Category Managment", admin_categories_path
	end

	def new
	  @category = Category.new
      add_crumb "Create Company", admin_categories_path
	end

	def edit
	  add_crumb "Update Category", admin_categories_path	
	end

	def create
	  @category = Category.new(category_params)
       if @category.save
        redirect_to admin_categories_path, notice: 'Category was successfully created.'
      else
        render :new
      end
	end

	def update
	  if @company.update(company_params)
  	    redirect_to admin_categories_path, notice: 'Category was successfully updated.'
	  else
  		render :edit
	  end
	end

	private

	def category_params
		params.require(:category).permit(:name,:parent_id)
	end

end