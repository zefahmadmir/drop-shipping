class SessionsController < ApplicationController

def new
  end

  def create
  	user = login(params[:email], params[:password], true)
    if user.present?
      flash[:success] = "Signed In Successfully"
      redirect_back_or_to root_url
    else
      flash[:error] = "Incorrect Email or Password."
      render :new
    end
  end

  def destroy
  	logout
    flash[:success] = "You have been logged out Successfully."
  	redirect_to root_path
  end
end
