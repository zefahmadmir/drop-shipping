class AdminController < ApplicationController
	before_action :require_login
	authorize_resource
	layout 'admin'
	before_action :set_crumb

	def index
	end	

	def set_crumb
    	add_crumb "Admin", admin_index_path
	end	

end
