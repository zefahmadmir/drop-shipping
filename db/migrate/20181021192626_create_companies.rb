class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.string :name
      t.integer :company_type
      t.string :uid
      t.integer :city_id
      t.integer :owner_id
      t.integer :status
      t.text :address

      t.timestamps
    end
  end
end
