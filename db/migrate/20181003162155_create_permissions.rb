class CreatePermissions < ActiveRecord::Migration[5.2]
  def change
    create_table :permissions do |t|
      t.string :name
      t.string :action
      t.string :klass
      t.text :condition

      t.timestamps
    end
  end
end
